﻿using UnityEngine;
using System.Collections;


public class PushTrigger : Trigger {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "arm" && other.gameObject.GetComponentInParent<Movement>().IsHitting)
        {
            obstacle.Trigger();
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.tag == "arm" && other.gameObject.GetComponentInParent<Movement>().IsHitting)
        {
            obstacle.Trigger();
        }
    }


}
