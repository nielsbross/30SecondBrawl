﻿using UnityEngine;
using System.Collections;

public class ScalingPlatform : MonoBehaviour {

    public float minSize;
    public float scaling;
    public bool shouldScale = false;
    public GameObject topWall, bottomWall, leftWall, rightWall;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (shouldScale)
        {
            if (transform.localScale.x > minSize && transform.localScale.y > minSize)
                transform.localScale -= new Vector3(scaling, scaling, 0);

            topWall.transform.position = new Vector2(topWall.transform.position.x, transform.position.y - (GetComponent<BoxCollider2D>().bounds.size.y / 2));
            bottomWall.transform.position = new Vector2(bottomWall.transform.position.x, transform.position.y + (GetComponent<BoxCollider2D>().bounds.size.y / 2));
            leftWall.transform.position = new Vector2(transform.position.x - (GetComponent<BoxCollider2D>().bounds.size.y / 2), leftWall.transform.position.y);
            rightWall.transform.position = new Vector2(transform.position.x - (GetComponent<BoxCollider2D>().bounds.size.y / 2), rightWall.transform.position.y);
        }
	}
}
