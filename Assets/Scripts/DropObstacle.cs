﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DropObstacle : MonoBehaviour 
{

    public Text winnerText;

	void Start () 
    {
	    
    }


	
	void Update () 
    {
	
	}

    public void Drop()
    {
        StartCoroutine(DropDown());
    }

    IEnumerator DropDown()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y, -2);
        for (int i = 0; i < 15; i++)
        {
            transform.localScale -= new Vector3(0.5f, 0.5f, 0);
            yield return new WaitForSeconds(0.06f);
        }
        GetComponent<BoxCollider2D>().enabled = true;
        yield return new WaitForSeconds(0.1f);
        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<BoxCollider2D>().enabled = false;
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.tag == "player1")
        {
            winnerText.text = "Player 1 won!";
            other.gameObject.SetActive(false);
            StartCoroutine(Restart());
            
        }
        else if(other.gameObject.tag == "Player2")
        {
            winnerText.text = "Player 2 won!";
            other.gameObject.SetActive(false);
            StartCoroutine(Restart());
        }
    }

    IEnumerator Restart()
    {
        Debug.Log("FUCK");
        yield return new WaitForSeconds(1.5f);
        Application.LoadLevel(Application.loadedLevel);

    }
}
