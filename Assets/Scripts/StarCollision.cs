﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StarCollision : MonoBehaviour {


    public Text player1Score;
    public Text player2Score;
    public Movement p1;
    public Movement p2;
    float p1Time = 0.0f;
    float p2Time = 0.0f;
    bool p1Touching = false;
    bool p2Touching = false;
    public float winTime;
    int p1Score = 0;
    int p2Score = 0;
    bool IsRestarting { get; set; }
    public Sprite[] coloredStars;

	void Start () 
    {
        transform.position = new Vector3(transform.position.x, transform.position.y, -2);
        player1Score.text = p1Score + "";
        player2Score.text = "- " + p2Score;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (p1Score == 3 || p2Score == 3)
            Application.LoadLevel("PlatformLevel");
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "player1" && !p2Touching)
        {
            p1Touching = true;
            GetComponent<SpriteRenderer>().sprite = coloredStars[1];
            CountPlayer(other);
        }
        else if (other.gameObject.tag == "player2" && !p1Touching)
        {
            p2Touching = true;
            GetComponent<SpriteRenderer>().sprite = coloredStars[2];
            CountPlayer(other);
        }
        else {
            GetComponent<SpriteRenderer>().sprite = coloredStars[0];
        }
        
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if(other.gameObject.tag == "player1" && !p2Touching)
        {
            p1Touching = true;
            GetComponent<SpriteRenderer>().sprite = coloredStars[1];
            CountPlayer(other);
        }
        else if (other.gameObject.tag == "player2" && !p1Touching)
        {
            p2Touching = true;
            GetComponent<SpriteRenderer>().sprite = coloredStars[2];
            CountPlayer(other);
        }
        else {
            GetComponent<SpriteRenderer>().sprite = coloredStars[0];
        }
        
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "player1") { p1Touching = false; GetComponent<SpriteRenderer>().sprite = coloredStars[0]; }
        else if (other.gameObject.tag == "player2") { p2Touching = false; GetComponent<SpriteRenderer>().sprite = coloredStars[0]; }
    }

    void CountPlayer(Collider2D other)
    {
        if (other.gameObject.tag == "player1" && !IsRestarting && !other.gameObject.GetComponent<JumpController>().jumped && p1Touching)
        {
            
            if (p1Time >= winTime)
            {
                player1Score.text = "" + ++p1Score;
                Restart();
                StartCoroutine(RestartLevel());
                p1Time = 0.0f;
                p2Time = 0.0f;
                p1Touching = p2Touching = false;
            }
            else
            {
                p1Time += Time.deltaTime;
            }
        }
        else if (other.gameObject.tag == "player2" && !IsRestarting && !other.gameObject.GetComponent<JumpController>().jumped && p2Touching)
        {
            if (p2Time >= winTime)
            {
                player2Score.text = "" + ++p2Score;
                Restart();
                StartCoroutine(RestartLevel());
                p1Time = 0.0f;
                p2Time = 0.0f;
                p1Touching = p2Touching = false;
            }
            else
            {
                p2Time += Time.deltaTime;
            }
        }
    }

    IEnumerator RestartLevel()
    {
        IsRestarting = true;
        yield return new WaitForSeconds(1f);
        IsRestarting = false;
    }
    internal void Restart()
    {
        p1.Restart();
        p2.Restart();
    }
}

