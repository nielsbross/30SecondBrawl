﻿using UnityEngine;
using System.Collections;
using System;

public class GenericObstacle : Obstacle {

	// Use this for initialization
	void Start ()
    {
        TriggerTime = 4.0f;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Triggered && TriggerTime > 0.0f)
        {
            TriggerTime -= Time.deltaTime;
            Debug.Log("Obstacle running");
        } else if (TriggerTime <= 0.0f && Triggered)
        {
            Triggered = false;
            ResetTriggerTime();
        }
	}

    public override void Trigger()
    {
        Triggered = true;
        Debug.Log("Triggered obstacle!");
    }

    public override void ResetTriggerTime()
    {
        TriggerTime = 7.5f;
        Debug.Log("Obstacle finished");
    }
    
}
