﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ObstacleSpawner : MonoBehaviour 
{

    const int OBSTACLE_COUNT = 60;
    int index = 0;
    public GameObject obstacle;
    GameObject[] obstacles;
    public BoxCollider2D collider;
    public Text text;

    void Start()
    {
       
        obstacles = new GameObject[OBSTACLE_COUNT];
        for (int i = 0; i < OBSTACLE_COUNT; i++)
        {
            obstacles[i] = Instantiate(obstacle, transform.position, Quaternion.identity) as GameObject;
            obstacles[i].GetComponent<DropObstacle>().winnerText = text;
            obstacles[i].SetActive(false);
        }
        StartCoroutine(Spawn());
    }
	
	void Update () 
    {
	    
	}

    IEnumerator Spawn()
    {
        Debug.Log("INSTANTIATE");
        Vector2 pos = new Vector2(Random.Range(collider.bounds.min.x, collider.bounds.max.x), Random.Range(collider.bounds.min.y, collider.bounds.max.y));
        obstacles[index].transform.position = pos;
        obstacles[index].transform.localScale = obstacles[index].transform.localScale * Random.Range(10, 14);
        obstacles[index].SetActive(true);
        obstacles[index++].GetComponent<DropObstacle>().Drop();
        yield return new WaitForSeconds((float)Random.Range(1, 2) / 2f);

        if (index == obstacles.Length)
            Application.LoadLevel("ObstacleTestScene");

        StartCoroutine(Spawn());
    }
}
