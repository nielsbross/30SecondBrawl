﻿using UnityEngine;
using System.Collections;

public abstract class Obstacle : MonoBehaviour {

    public abstract void Trigger();
    public abstract void ResetTriggerTime();

    public bool Take()
    {
        if (Taken)
        {
            return false;
        }
        else
        {
            Taken = true;
            return true;
        }
    }

    public float TriggerTime { get; set; }
    public bool Triggered { get; set; }

    [System.ComponentModel.DefaultValue(false   )]
    protected bool Taken { get; set; }

   

}
