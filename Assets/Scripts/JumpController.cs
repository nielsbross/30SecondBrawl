﻿using UnityEngine;
using System.Collections;

public class JumpController : MonoBehaviour {

    Vector3 initScale;
    public float jumpHeight;
    public bool jumped = false;
    float initCircleBounds;
    BoxCollider2D[] armColls;

    // Use this for initialization
    void Start () {
        initScale = transform.localScale;
        initCircleBounds = GetComponent<CircleCollider2D>().radius;
        armColls = GetComponentsInChildren<BoxCollider2D>();
	}
	
	// Update is called once per frame
	void Update () {
	}

    public IEnumerator Jump()
    {
        jumped = true;
        SetArmColls(false);
        GetComponent<Movement>().onWalkable = false;
        GetComponent<CircleCollider2D>().enabled = false;
        transform.position = new Vector3(transform.position.x, transform.position.y, -3);
        GetComponent<AudioSource>().Play();
        for (int i = 0; i < 4; i++)
        {
            yield return new WaitForSeconds(0.05f);
            
            transform.localScale += new Vector3(jumpHeight, jumpHeight, 0);
            GetComponent<CircleCollider2D>().radius -= jumpHeight;
        }

        for (int i = 0; i < 4; i++)
        {
            yield return new WaitForSeconds(0.05f);
            transform.localScale -= new Vector3(jumpHeight, jumpHeight, 0);
            GetComponent<CircleCollider2D>().radius = initCircleBounds;
        }
        
        transform.position = new Vector3(transform.position.x, transform.position.y, -2);
        SetArmColls(true);
        GetComponent<CircleCollider2D>().enabled = true;
        
        yield return new WaitForSeconds(0.1f);
        jumped = false;
    }

    void SetArmColls(bool newVal)
    {
        foreach (BoxCollider2D b in armColls)
        {
            b.enabled = newVal;
        }
    }

}
