﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure;

public class Movement : MonoBehaviour
{
    const float SPEED = 5;
    GamePadState state;
    GamePadState prevState;
    public int PlayerNr;
    public Transform leftArm;
    public Transform rightArm;
    public bool onWalkable = true;
    Vector3 initPos;

    bool JumpReleased = true;
    bool HitReleased = true;
    bool IsLeft = false;
    public bool IsHitting = false;
    public PlayerIndex PlayerIndex { get { return (PlayerIndex)PlayerNr; }}
    public bool IsConnected { get; set; }
    float velX, velY;

	void Start ()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y, -2);
        initPos = transform.position;
	}
	
	void Update ()
    {
        if (!IsConnected && !prevState.IsConnected)
        {
            GamePadState testState = GamePad.GetState(PlayerIndex);
            if(testState.IsConnected)
            {
                Debug.Log(string.Format("Connected controller {0}", PlayerIndex));
                IsConnected = true;
            }
        }

        prevState = state;
        state = GamePad.GetState(PlayerIndex);

        //Debug.Log(string.Format("x: {0} y: {1}", state.ThumbSticks.Left.X, state.ThumbSticks.Left.Y));

        velX = SPEED * state.ThumbSticks.Left.X;
        velY = SPEED * state.ThumbSticks.Left.Y;

        float angle = Mathf.Atan2(state.ThumbSticks.Left.X, state.ThumbSticks.Left.Y) * Mathf.Rad2Deg * - 1;
        
        if(angle != 0)
            transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));

        //Debug.Log(string.Format("xvel: {0} yvel: {1}", velX, velY));

        //if (!GetComponent<JumpController>().jumped)
            transform.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(velX, velY);
        //else
        //{
        //    Vector2 vel = transform.gameObject.GetComponent<Rigidbody2D>().velocity;
        //    transform.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(vel.x + velX / 10, vel.y + velY / 10);
        //}
        if (HitReleased && prevState.Buttons.X == ButtonState.Pressed && state.Buttons.X == ButtonState.Pressed && !IsHitting)
        {
            StartCoroutine(Hit());
            HitReleased = false;
        }
        if (prevState.Buttons.X == ButtonState.Released && state.Buttons.X == ButtonState.Released)
            HitReleased = true;

        if (JumpReleased && prevState.Buttons.A == ButtonState.Pressed && state.Buttons.A == ButtonState.Pressed)
        {
            if (!GetComponent<JumpController>().jumped)
            {
                StartCoroutine(GetComponent<JumpController>().Jump());
                JumpReleased = false;
            }
        }

        if (prevState.Buttons.A == ButtonState.Released && state.Buttons.A == ButtonState.Released)
            JumpReleased = true;
        
        if(!onWalkable && !GetComponent<JumpController>().jumped)
        {
            Death();
        }


	}

    IEnumerator Hit()
    {
        IsHitting = true;
        if (!IsLeft)
        {
            StartCoroutine(AnimateArm(true));
            IsLeft = true;
        }
        else
        {
            StartCoroutine(AnimateArm(false));
            IsLeft = false;
        }
        yield return new WaitForSeconds(0);
    }

    IEnumerator AnimateArm(bool left)
    {
        Transform arm = left ? leftArm : rightArm;

        float x = arm.localPosition.x;
        float y = arm.localPosition.y;
        
        for (int i = 0; i < 2; i++)
        {
            yield return new WaitForSeconds(0.03f);
            y += 0.1f;
            arm.localPosition = new Vector2(x, y);
        }

        for (int i = 0; i < 2; i++)
        {
            yield return new WaitForSeconds(0.03f);
            y -= 0.1f;
            arm.localPosition = new Vector2(x, y);
        }
        IsHitting = false;
    }
    
    void Death()
    {
        transform.position = initPos;
    }


    void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.tag == "WalkableArea")
        {
            onWalkable = true;
        }
        //Debug.Log(string.Format("Hit by: {0}", other.gameObject.tag));

        
        if(other != null && other.gameObject != null && other.gameObject.GetComponentInParent<Movement>() != null)
            if (other.gameObject.GetComponentInParent<Movement>().IsHitting)
            {
                Vector2 dir = (transform.position - other.transform.position).normalized;
                gameObject.GetComponent<Rigidbody2D>().AddForce(dir * 500);
            }

        if (other.gameObject.tag.Substring(0, 6) == "player")
        {
            GameObject coll = other.gameObject;

            if (coll.GetComponentInParent<Movement>().IsHitting && coll.GetComponentInParent<JumpController>().jumped == gameObject.GetComponent<JumpController>().jumped)
            {
                Vector2 dir = (transform.position - other.transform.position).normalized;
                gameObject.GetComponent<Rigidbody2D>().AddForce(dir * 500);
            }
            else if (coll.GetComponentInParent<JumpController>().jumped != gameObject.GetComponent<JumpController>().jumped)
            {
                Physics2D.IgnoreCollision(coll.GetComponent<Collider2D>(), GetComponent<Collider2D>());
            }
        }
        GetComponent<Rigidbody2D>().freezeRotation = true;
        
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.tag == "WalkableArea")
        {
            onWalkable = true;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "WalkableArea")
        {
            onWalkable = true;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "WalkableArea")
        {
            onWalkable = false;
        }
    }

    public void Restart()
    {
        Death();
    }

}
