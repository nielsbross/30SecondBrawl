﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {

    public List<Obstacle> obstacles;
    public List<Trigger> triggers;

	// Use this for initialization
	void Start () {
        Random.seed = System.Environment.TickCount;
        foreach (Trigger t in triggers)
        {
            Obstacle ob = obstacles[(int)Random.Range(0,Mathf.Max(0,obstacles.Count - 1))];
            if (ob.Take())
            {
                t.GiveObstacle(ob);
                obstacles.Remove(ob);
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
