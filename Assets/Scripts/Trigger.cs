﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (BoxCollider2D))]
public class Trigger : MonoBehaviour {

    public Obstacle obstacle;

    public void GiveObstacle(Obstacle o)
    {
        obstacle = o;
    }
}
